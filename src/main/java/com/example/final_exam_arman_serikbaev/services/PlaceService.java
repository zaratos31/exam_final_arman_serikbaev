package com.example.final_exam_arman_serikbaev.services;


import com.example.final_exam_arman_serikbaev.models.Place;
import com.example.final_exam_arman_serikbaev.repositories.PlaceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PlaceService {
    private final PlaceRepository placeRepository;

    public void save(String title, String description, MultipartFile multipartFile){
        Place place = new Place();
        place.setName(title);
        place.setDescription(description);
        place.setMainImage(getImageFilePath(multipartFile));
        placeRepository.save(place);
    }

    public List<Place> getPlaces(){
        return placeRepository.findAll();
    }

    public String getImageFilePath(MultipartFile file){
        String imageFilePath = "";
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        if (filename.contains("..")){
            System.out.println("not a valid file");
        }
        try{
            imageFilePath = Base64.getEncoder().encodeToString(file.getBytes());
        }catch (IOException e){
            e.printStackTrace();
        }
        return imageFilePath;
    }

}
