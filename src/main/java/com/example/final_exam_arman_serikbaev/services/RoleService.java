package com.example.final_exam_arman_serikbaev.services;

import com.example.final_exam_arman_serikbaev.models.Role;
import com.example.final_exam_arman_serikbaev.repositories.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RoleService {
    private final RoleRepository roleRepository;

    public void save(Role role){
        roleRepository.save(role);
    }
}
