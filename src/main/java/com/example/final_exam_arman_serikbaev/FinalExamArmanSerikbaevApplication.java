package com.example.final_exam_arman_serikbaev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalExamArmanSerikbaevApplication {

    public static void main(String[] args) {
        SpringApplication.run(FinalExamArmanSerikbaevApplication.class, args);
    }

}
