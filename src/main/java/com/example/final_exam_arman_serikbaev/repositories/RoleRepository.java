package com.example.final_exam_arman_serikbaev.repositories;

import com.example.final_exam_arman_serikbaev.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByName(String name);
}
