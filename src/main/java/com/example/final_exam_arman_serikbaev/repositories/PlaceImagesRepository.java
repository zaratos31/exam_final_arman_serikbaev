package com.example.final_exam_arman_serikbaev.repositories;

import com.example.final_exam_arman_serikbaev.models.PlaceImages;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlaceImagesRepository extends JpaRepository<PlaceImages, Integer> {
}
