package com.example.final_exam_arman_serikbaev.repositories;

import com.example.final_exam_arman_serikbaev.models.Place;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlaceRepository extends JpaRepository<Place, Integer> {

}
