package com.example.final_exam_arman_serikbaev.repositories;

import com.example.final_exam_arman_serikbaev.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUsername(String username);
}
