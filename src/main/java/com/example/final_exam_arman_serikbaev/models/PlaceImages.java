package com.example.final_exam_arman_serikbaev.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "images_of_places")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PlaceImages {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Lob
    @Column(columnDefinition = "MEDIUMBLOB")
    private String image;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "place_id")
    private Place place;


}
