package com.example.final_exam_arman_serikbaev.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "places")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Place {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotBlank
    @Column(length = 128)
    private String name;

    @NotBlank
    @Column(length = 128)
    private String description;

    @Lob
    @Column(columnDefinition = "MEDIUMBLOB")
    private String mainImage;

    private Double rating;


    @OneToMany(mappedBy = "place")
    private Set<PlaceImages> images;



}
