package com.example.final_exam_arman_serikbaev.controllers;


import com.example.final_exam_arman_serikbaev.services.PlaceService;
import com.example.final_exam_arman_serikbaev.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.security.Principal;

@Controller
@RequiredArgsConstructor
public class MainController {
    private final UserService userService;
    private final PlaceService placeService;

    @GetMapping("/")
    public String index(){
    return "main";
    }

    @PostMapping("/register")
    public String userRegister(@RequestParam("username") String username,
                               @RequestParam("password") String password){
        userService.save(username, password);
        return "redirect:/register_success";
    }

    @GetMapping("/main")
    public String getMain(Model model, Principal principal){
        model.addAttribute("places", placeService.getPlaces());
        model.addAttribute("username", principal.getName());
        return "authorize_main";
    }

    @GetMapping("/register_success")
    public String getRegisterSuccess(){
        return "register_success";
    }

    @GetMapping("/addPlace")
    public String getAddPlacePage(Model model, Principal principal){
        model.addAttribute("username", principal.getName());
        return "addPlace";
    }

    @PostMapping("/addPlace")
    public String addPlace(@RequestParam("title")String title,
                           @RequestParam("description") String description,
                           @RequestParam("photo")MultipartFile file){
        return "redirect:/main";
    }
}
