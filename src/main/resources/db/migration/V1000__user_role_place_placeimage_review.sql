use `final_exam_arman_serikbaev`;
create table users (
        id integer auto_increment NOT NULL,
        password varchar(128),
        username varchar(128),
        primary key (id)
    );


create table roles (
       id integer auto_increment NOT NULL,
        name varchar(128),
        primary key (id)
    );


create table users_roles (
       user_id integer not null,
        roles_id integer not null
    );


create table places (
       id integer auto_increment NOT NULL,
        description varchar(128),
        main_image MEDIUMBLOB,
        name varchar(128),
        rating double precision,
        primary key (id)
    );


create table reviews (
       id integer auto_increment NOT NULL,
        rating double precision,
        text varchar(255),
        primary key (id)
    );


create table images_of_places (
       id integer auto_increment NOT NULL,
        image MEDIUMBLOB,
        place_id integer,
        primary key (id)
    );

insert into users (password,username)
values ('$2a$12$z22aZs4mt9B0nRXSqItXjO1ravMRWqXZxkBJ7gKOIyPnZNqo4BQNK','admin');

insert into roles (name)
values ('ROLE_ADMIN'), ( 'ROLE USER');

insert into users_roles(user_id, roles_id) values ('1','1');